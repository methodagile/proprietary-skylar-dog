<?php 
/**
 * The template for displaying the header
 *
 * @package Standard
 * @since 	3.0
 * @version	3.0
 */
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html id="ie8" <?php  language_attributes(); ?>><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php  language_attributes(); ?>><!--<![endif]-->
	<head>	
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width">
		
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php  bloginfo( 'pingback_url' ); ?>" />
		<title><?php  wp_title( '' ); ?></title>
		<?php  $presentation_options = get_option( 'standard_theme_presentation_options'); ?>
		<?php  if( '' != $presentation_options['fav_icon'] ) { ?>
			<link rel="shortcut icon" href="<?php  echo $presentation_options['fav_icon']; ?>" />
			<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php  echo $presentation_options['fav_icon']; ?>" />
		<?php  } // end if ?>
		<?php  global $post; ?>
		<?php  if( standard_using_native_seo() && ( ( is_single() || is_page() ) && ( 0 != strlen( trim( ( $google_plus = get_user_meta( $post->post_author, 'google_plus', true ) ) ) ) ) ) ) { ?>
			<?php  if( false != standard_is_gplusto_url( $google_plus ) ) { ?>
				<?php  $google_plus = standard_get_google_plus_from_gplus( $google_plus ); ?>
			<?php  } // end if ?>
			<link rel="author" href="<?php  echo trailingslashit( $google_plus ); ?>"/>
		<?php  } // end if ?>
		<?php  $global_options = get_option( 'standard_theme_global_options' ); ?>
		<?php  if( '' != $global_options['google_analytics'] ) { ?>
			<?php  if( is_user_logged_in() ) { ?>
				<!-- Google Analytics is restricted only to users who are not logged in. -->
			<?php  } else { ?>
				<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', '<?php  echo $global_options[ 'google_analytics' ] ?>']);
					_gaq.push(['_trackPageview']);
					_gaq.push(['_trackPageLoadTime']);

					<?php  if( 0 != strlen( $global_options[ 'google_analytics_domain'] ) ) { ?>
						_gaq.push(['_setDomainName', '<?php  echo $global_options[ 'google_analytics_domain' ] ?>']);
					<?php  } // end if/else ?>
					
					<?php  if( 1 == $global_options[ 'google_analytics_linker'] ) { ?>
						_gaq.push(['_setAllowLinker', true]);
					<?php  } // end if/else ?>
					
					(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})();
				</script>
			<?php  } // end if/else ?>
		<?php  } // end if ?>
		<?php  if( standard_google_custom_search_is_active() ) { ?>
			<?php  $gcse = get_option( 'widget_standard-google-custom-search' ); ?>
			<?php  $gcse = array_shift( array_values ( $gcse ) ); ?>
			<script type="text/javascript">
			  (function() {
			    var cx = '<?php  echo trim( $gcse['gcse_content'] ); ?>';
			    var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
			    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
			        '//www.google.com/cse/cse.js?cx=' + cx;
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
			  })();
			</script>
		<?php  } // end if ?>
		<?php  wp_head(); ?>
		<link href='http://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>
	</head>
	<body <?php  body_class(); ?> topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
		<div class="centered">
		<!-- start header -->
			<table width="1264" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100" style="width:100;vertical-align:top;text-align:right;background-image:url('http://www.skylardog.com/skin/frontend/default/skylar/images/body_gradient_left.png');background-repeat:repeat-y;background-position:top right;"><img src="http://www.skylardog.com/skin/frontend/default/skylar/images/blank.gif" width="100" height="1" /></td>
					<td width="864" style="background-color:#91bdf6;">
						<table>
							<tr>
								<td height="137">
									 <table width="864" height="126" border="0" cellpadding="0" cellspacing="0">
									 	<tbody><tr>
											<td width="289"><a href="http://www.skylardog.com/" title="Skylar and George Washington"><img src="/skin/frontend/default/skylar/images/skylardog.gif" border="0" title="Skylar and George Washington" alt="Skylar and George Washington"></a></td>
											<td width="145"><a href="/the-book" title="Skylar the Book"><img src="/skin/frontend/default/skylar/images/menu_book.gif" border="0" title="Skylar the Book" alt="Skylar the Book"></a></td>
											<td width="128"><a href="/the-dog" title="Skylar the Dog"><img src="/skin/frontend/default/skylar/images/menu_dog.gif" border="0" title="Skylar the Dog" alt="Skylar the Dog"></a></td>
											<td width="137"><a href="/the-hero" title="George Washington the Hero"><img src="/skin/frontend/default/skylar/images/menu_hero.gif" border="0" title="George Washington the Hero" alt="George Washington the Hero"></a></td>
											<td width="165"><a href="/the-author" title="Matt Burgess the Author"><img src="/skin/frontend/default/skylar/images/menu_author.gif" border="0" title="Matt Burgess the Author" alt="Matt Burges the Author"></a></td>
										</tr>
									</tbody></table>
								</td>
							</tr>
						</table>
		<?php  if( standard_is_offline() && ! current_user_can( 'manage_options' ) ) { ?>
			<?php  get_template_part( 'page', 'offline-mode' ); ?>
			<?php  exit; ?>
		<?php  } // end if ?>
		
		<?php  get_template_part( 'lib/breadcrumbs/standard_breadcrumbs' ); ?>
			

<?php 
/**
 * The template for rendering the footer.
 *
 * @package Standard
 * @since 	3.1
 * @version	3.0
 */
?>

		<div id="footer" class="clearfix">
			<div id="sub-floor" class="clearfix">
				<div class="container">
					<div class="row-fluid">
						<div class="span12">
								<?php   
									if( has_nav_menu( 'footer_menu' ) && ( ! standard_is_offline() || is_user_logged_in() ) ) {
										wp_nav_menu( 
											array(
												'theme_location'  	=> 'footer_menu',
												'container_class' 	=> 'menu-footer-nav-container navbar',
												'items_wrap'      	=> '<ul id="%1$s" class="nav %2$s">%3$s</ul>',
												'fallback_cb'		=> false,
												'depth'          	=> 1
											)
										); 	
									} // end if 
								?>
						</div><!--/span12-->
					</div><!-- /row -->
				</div><!-- /.container -->
			</div><!-- /#sub-floor -->
		</div><!-- /#footer -->
		<?php  wp_footer(); ?>
		</td>
		<td width="100" style="width:100;vertical-align:top;text-align:left;background:url('http://www.skylardog.com/skin/frontend/default/skylar/images/body_gradient_right.png');background-repeat:repeat-y;background-position:top left;"><img src="http://www.skylardog.com/skin/frontend/default/skylar/images/blank.gif" width="100" height="1" /></td>
	</tr>
</table>
</div>
	</body>
</html>